import { useState } from 'react';
import { Form, Button, Row, Col, Card } from 'react-bootstrap';


export default function Calc(){

	const [number1, setNum1] = useState("");
	const [number2, setNum2] = useState("");
	const [ans, setAns] = useState("")

	function Add() {
		setAns(number1+number2)
	}

	function Subtract() {
		setAns(number1-number2)
	}

	function Multiply() {
		setAns(number1*number2)
	}

	function Divide() {
		setAns(number1/number2)
	}

	function Clear() {
		setAns(0)
	}


	function performMath(e){
		e.preventDefault()//prevent default form behavior so form does not submit

		setNum1(0)
		setNum2(0)


		// if (ans = true){
		// 	setAns === number1 + number2
		// }
		alert('Login Successful')
	}

	return (

		<>
		<h1 className="mb-2 text-center">Calculator</h1>
		<Form onSubmit={e => performMath(e)}>
		<Card className="mb-2 text-center">
		    <Card.Body>
		        <Card.Title>
		            {ans}
		         </Card.Title>
		    </Card.Body>
		</Card>
		<Row className="mb-3">
			<Form.Group as={Col} controlId="number1">
				<Form.Control
					className="mb-2 text-center"
					type="number"
					placeholder="Enter number"
					value={number1}
					onChange={e => setNum1(parseInt(e.target.value))}
					required
				/>
			</Form.Group>
		
			<Form.Group as={Col} controlId="number2">
				<Form.Control
					className="mb-2 text-center"
					type="number"
					placeholder="Enter number"
					value={number2}
					onChange={e => setNum2(parseInt(e.target.value))}
					required
				/>
			</Form.Group>
		</Row>
		<Row className="mb-3">
				<>
					<Button as={Col}className="mt-3 me-3 ms-3" variant="primary" onClick={Add}>
					Add
					</Button>
					<Button as={Col} className="mt-3 me-3" variant="primary" onClick={Subtract}>
					Subtract
					</Button>
					<Button as={Col} className="mt-3 me-3" variant="primary" onClick={Multiply}>
					Multiply
					</Button>
					<Button as={Col} className="mt-3 me-3" variant="primary" onClick={Divide}>
					Divide
					</Button>
					<Button as={Col} className="mt-3 me-3" variant="primary" onClick={Clear}>
					Clear
					</Button>

				</>
					
				
		</Row>
		</Form>
		</>

	)
}



