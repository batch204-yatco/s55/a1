import { useEffect, useState } from 'react';
// import courseData from '../data/courseData';
import CourseCard from '../components/CourseCard';


export default function Courses(){

	const [coursesData, setCoursesData] = useState([])

	// Check if mock data was captured
	// console.log(courseData);
	// console.log(courseData[0]);

	// Props
		// Shorthand for "property" since components are considered as object in ReactJS
		// Way to pass data from parent component to child component
		// Synonymous to the function parameter
		//courseProp is user defined

	// map will loop through data then generate array of courese

	// Fetch by default always makes aa GET request, unless a different one is specified
	// ALWAYS add fetch requests for getting data in a useEffect hook
	useEffect(() => {
		// console.log(process.env.REACT_APP_API_URL)
		// changes to env files are applied ONLY at build time when starting the project locally

		fetch(`${process.env.REACT_APP_API_URL}/courses`)
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			setCoursesData(data)
		})
	}, [])

	const courses = coursesData.map(course => {
		return (
			<CourseCard courseProp={course} key={course._id}/>
		)
	})

	return (
		<>
			<h1>Courses</h1>
			{courses}
		</>
	)
}